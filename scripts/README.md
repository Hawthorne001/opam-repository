## Build the Docker images locally

To build the Docker images locally, run:

```sh
./scripts/create_docker_images.sh
```

from the root of this repository.

If you forked the repository on GitLab, instead use:

```sh
./scripts/create_docker_images.sh registry.gitlab.com/<your_gitlab_username>/opam-repository
```
